const express = require('express')
const bodyParser = require('body-parser')

const app = express()
const PORT = 3000
var serialPort = null;
app.use(express.static('./src/public'))
app.use(bodyParser.json())

app.post('/move', (req, res) => {
  console.log('Command received on server:',req.body.command);
  if(!serialPort){
    return res.send({
      status: 'Failure'
    });
  }

  serialPort.write(req.body.command);
  res.send({
    status: "Success",
    action: req.body.command
  })
  
})

app.post('/connect', (req, res) => {
  connectBluetooth()
  res.send({
    status: "Success"
  })
  
})

app.listen(PORT, () => {
  console.log('Server is listening on port ', PORT)
})


function connectBluetooth(){
var SerialPort = require('serialport')
var Readline = SerialPort.parsers.Readline
serialPort = new SerialPort("COM4", {
  baudRate: 9600
})

var parser = new Readline()
serialPort.pipe(parser)
parser.on('data', function (data) {
  console.log('data received: ' + data)
})

serialPort.on('open', function () {
  console.log('Communication is on!')
})
}