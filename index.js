function sendCommand (cmd) {
  console.log('Sending command', cmd)

  fetch('/move', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      command: cmd
    })
  })
    .then(response => {
      if (response && response.status === 200) {
        return response.json()
      }
      else {
        console.error(response.statusText)
      }
    })
    .then(json => {
      console.log(json)
    })
}
var state = null;
function connect () {
if (state==1){
  return;
}
  fetch('/connect', {
    method: 'POST'
  })
    .then(response => {
      if (response && response.status === 200) {
        return response.json()
      }
      else {
        console.error(response.statusText)
      }
    })
    .then(json => { //though this is getting called
      console.log(json)
      setAsConnected() //this is not getting called
    })
}

function setAsConnected () {
  document.getElementById('connect').classList.remove('red')
  document.getElementById('connect').classList.add('green')
  state = 1;
}


let cmd = null;
function registerListeners () {
  document.addEventListener('mousedown', function (e) {
    if (e.target && e.target.tagName === 'IMG') {
      cmd = e.target.getAttribute('data-cmd')
      console.log('Mouse down.Sending: ', cmd);

      sendCommand(cmd);
    }
  })

  document.addEventListener('mouseup', function (e) {
    if (e.target && e.target.tagName === 'IMG') {
      cmd = null;
      console.log('Mouse up ');
      sendCommand("static")
    }
  })

  document.getElementById('connect').addEventListener('click', function (e) {
    connect()
  })
}

function init () {
  console.log('initializing')
  registerListeners()
}

function connectBluetooth() {

    const SerialPort = require('serialport');
    const Readline = SerialPort.parsers.Readline;
    const port = new SerialPort('COM4', {
      baudRate: 9600,
    }, function(err) {
      if (err){
        console.log('error: ', err.message);
        port.close();
    }});
    const parser = new Readline({delimiter: '\n'});
    port.pipe(parser);
    
    console.log('parser setup');
    
    parser.on('data', function(data) {
      console.log('data received: ', data);
    });
}